package main

type Cart struct {
	entries      []CartEntry
	deliveryCost float64
}

type Product struct {
	Name        string
	Description string
}

type CartEntry struct {
	Product  Product
	Quantity int
	Price    float64
}

type CustomerProfile struct {
	FreeDeliverOver  float64
	StandardDiscount float64
}

func CalculateCartTotal(cart *Cart, profile *CustomerProfile) float64 {
	// calculate total
	total := 0.0
	for _, entry := range cart.entries {
		total += entry.Price * float64(entry.Quantity)
	}

	// apply deliver cost when needed
	if total < profile.FreeDeliverOver {
		total += cart.deliveryCost
	}

	// subtract standard discount
	total = total - (total * profile.StandardDiscount)

	return total
}
